package com.example.manish.locationfinderUserApp.intracters

import com.google.android.gms.tasks.Task
import com.google.firebase.firestore.QuerySnapshot

interface FirestoreListner {
    fun onFailure(message: String)
    fun onSuccessForSigleObject(it: Task<Void>)
    fun onSuccessForArrayObject(it: Task<QuerySnapshot>)
}