package com.example.manish.locationfinderUserApp.models

import io.realm.RealmModel
import io.realm.annotations.PrimaryKey
import io.realm.annotations.RealmClass
import java.util.*

@RealmClass
open class Information : RealmModel {

    @PrimaryKey
    var timestemp: Long = 0
    var contact_number: String = ""
    var contact_name: String = ""
    var duration: Long = 0
    var latlng: String = ""
    var message: String = ""
    var admin_email: String = ""
    var admin_identification: String = ""


    constructor() {

    }

    constructor(
        contact_number: String,
        contact_name: String,
        duration: Long,
        latlng: String,
        message: String,
        admin_email: String,
        admin_identification: String
    ) {
        this.contact_number = contact_number
        this.contact_name = contact_name
        this.timestemp = Date().time
        this.duration = duration
        this.latlng = latlng
        this.message = message
        this.admin_email = admin_email
        this.admin_identification = admin_identification

    }
}
