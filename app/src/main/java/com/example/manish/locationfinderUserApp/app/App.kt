package com.example.manish.locationfinderUserApp.app

import android.app.Application
import io.realm.Realm
import io.realm.RealmConfiguration

class App : Application() {
    val pref by lazy { getSharedPreferences("LocationFinder", 0) }
    override fun onCreate() {
        super.onCreate()
        application = this
        initRealm()
    }
    private fun initRealm() {
        Realm.init(this)
        val config = RealmConfiguration.Builder()
            .name("prontoschool.realm")
            .schemaVersion(1)
            .deleteRealmIfMigrationNeeded()
            .build()
        Realm.setDefaultConfiguration(config)

    }

    companion object {
        lateinit var application: App
    }


}