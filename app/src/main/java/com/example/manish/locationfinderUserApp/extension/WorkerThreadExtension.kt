package com.example.manish.locationfinderUserApp.extension


import android.os.Handler
import java.util.concurrent.TimeUnit

/**
 * Extension method to run block of code after specific Delay.
 */
fun Int.runDelayed(timeUnit: TimeUnit = TimeUnit.SECONDS, action: () -> Unit) {
    Handler().postDelayed(action, timeUnit.toMillis(this.toLong()))
}
