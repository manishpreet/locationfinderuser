package com.example.manish.locationfinderUserApp.intracters

import com.example.manish.locationfinderUserApp.keys.FirestoreKeys
import com.example.manish.locationfinderUserApp.models.Information
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.FirebaseFirestoreSettings

class FirestoreHelper {

    private val firestoreRef by lazy {
        FirebaseFirestore.getInstance().apply {
            firestoreSettings = FirebaseFirestoreSettings.Builder()
                .setPersistenceEnabled(true)
                .build()
        }
    }

    companion object {
        private val fireStoreHelper by lazy { FirestoreHelper() }
        fun getInstance(): FirestoreHelper {
            return fireStoreHelper
        }
    }

    fun getStoreRef() = firestoreRef
    fun clientRef() = getStoreRef().collection(FirestoreKeys.CLIENTS)
    fun clientDataRef() = getStoreRef().collection(FirestoreKeys.CLIENT_DATA)

    fun saveInformation(data: Information) = clientDataRef().document().set(data)

    fun validateIdentification(device_id: String?) =
        clientRef().whereEqualTo("device_id", device_id).get()


}