package com.example.manish.locationfinderUserApp.ui

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.provider.Settings
import com.example.manish.locationfinderUserApp.R
import com.example.manish.locationfinderUserApp.dialog.AutostartDialog
import com.example.manish.locationfinderUserApp.extension.*
import com.example.manish.locationfinderUserApp.keys.PrefrencesKeys
import kotlinx.android.synthetic.main.activity_splash.*

class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        if (getPrefrence(PrefrencesKeys.LOGED_IN,false))
        {
            try {
                openActivity<HomeActivity>()
                finish()
            }catch (e:Exception){
                hideApplication()
                finish()
            }
        }else{
            var intent=checkManufacturing()
            if (intent!==null){
            AutostartDialog.newInstance(intent).show(supportFragmentManager,AutostartDialog::class.java.simpleName)}
        }


        txtContinue.setOnClickListener {
            if (ispermissionGranted()) {
                if(isLocationEnabled(this) && getLocationMode()==3)
                {
                    openActivity<HomeActivity>()
                    finish()
                }
                else
                {
                    "please select high accuracy option".toast()
                    startActivity(Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS))
                }

            }else{
                ispermissionGranted()
            }
        }
    }
    private fun ispermissionGranted(): Boolean {
        var permissions=arrayOf(
            android.Manifest.permission.ACCESS_FINE_LOCATION,
            android.Manifest.permission.READ_CONTACTS,
            android.Manifest.permission.READ_PHONE_STATE)
        if (hasPermissions(permissions))
            return true
        else
            permissions.askPermissions(this)

        return false
    }
}
