package com.example.manish.locationfinderUserApp.dialog

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.view.*
import com.example.manish.locationfinderUserApp.R
import com.example.manish.locationfinderUserApp.extension.autostart
import kotlinx.android.synthetic.main.view_autostart.*


class AutostartDialog : DialogFragment() {


    companion object {
        var autoStartIntent:Intent?=null
        fun newInstance(intent: Intent): AutostartDialog {
            this.autoStartIntent = intent
            val fragment = AutostartDialog()
            return fragment

        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        dialog.window.setGravity(Gravity.CENTER)
        dialog.window!!.requestFeature(Window.FEATURE_NO_TITLE)
        return inflater.inflate(R.layout.view_autostart, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        txtOk.setOnClickListener {
            dismiss()
            autoStartIntent?.autostart(context!!)
        }

    }


}