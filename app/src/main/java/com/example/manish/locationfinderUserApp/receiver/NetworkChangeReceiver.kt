package com.example.manish.locationfinderUserApp.receiver

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import com.example.manish.locationfinderUserApp.extension.*
import com.example.manish.locationfinderUserApp.intracters.FirestoreDataGetter
import com.example.manish.locationfinderUserApp.intracters.FirestoreListner
import com.example.manish.locationfinderUserApp.models.Information
import com.google.android.gms.tasks.Task
import com.google.firebase.firestore.QuerySnapshot
import java.lang.Exception

class NetworkChangeReceiver : BroadcastReceiver() {
    val firestoreDataGetter by lazy { FirestoreDataGetter() }
    var index: Int = 0
    var list: MutableList<Information>? = null
    override fun onReceive(context: Context, intent: Intent) {

        list?.clear()
        checkStatus()
    }

    fun checkStatus() {

        if (isNetConnected()) {
            3.runDelayed() {
                try {
                    list = getUnsaveInformation()
                    if (list?.size !== 0)
                        storeData()
                    else
                        "list empty".log("list")
                }catch (e:Exception){
                    e.message?.toast()
                }


            }
        } else {
            "internet not connected".log("internet status")
        }
    }

    private fun storeData() {
        if (index < list?.size!!) {
            firestoreDataGetter.saveInformation(list!![index], object : FirestoreListner {
                override fun onFailure(message: String) {
                    message.toast()
                    index = index + 1
                    storeData()
                }

                override fun onSuccessForSigleObject(it: Task<Void>) {
                    list?.get(index)?.timestemp?.deleteInformation()
                        index = index + 1
                        storeData()


                }

                override fun onSuccessForArrayObject(it: Task<QuerySnapshot>) {
                }

            })
        }
    }
}
