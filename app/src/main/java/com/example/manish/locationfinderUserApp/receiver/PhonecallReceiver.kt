package com.example.manish.locationfinderUserApp.receiver

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.telephony.TelephonyManager
import com.example.manish.locationfinderUserApp.extension.*
import com.example.manish.locationfinderUserApp.intracters.FirestoreDataGetter
import com.example.manish.locationfinderUserApp.intracters.FirestoreListner
import com.example.manish.locationfinderUserApp.keys.Constants
import com.example.manish.locationfinderUserApp.keys.PrefrencesKeys
import com.example.manish.locationfinderUserApp.models.Client
import com.example.manish.locationfinderUserApp.models.Information
import com.google.android.gms.tasks.Task
import com.google.firebase.firestore.QuerySnapshot
import locationprovider.davidserrano.com.LocationProvider
import java.util.*

open class PhonecallReceiver : BroadcastReceiver(), FirestoreListner {

    val firestoreDataGetter by lazy { FirestoreDataGetter() }
    val client by lazy { getprefObject<Client>(PrefrencesKeys.CLIENT) }

    override fun onReceive(context: Context, intent: Intent) {
        initLocation(context = context)
        //We listen to two intents.  The new outgoing call only tells us of an outgoing call.  We use it to get the number.
        if (intent.action == "android.intent.action.NEW_OUTGOING_CALL") {
            savedNumber = intent.extras!!.getString("android.intent.extra.PHONE_NUMBER")
        } else {
            val stateStr = intent.extras!!.getString(TelephonyManager.EXTRA_STATE)
            val number = intent.extras!!.getString(TelephonyManager.EXTRA_INCOMING_NUMBER)
            var state = 0
            if (stateStr == TelephonyManager.EXTRA_STATE_IDLE) {
                state = TelephonyManager.CALL_STATE_IDLE
            } else if (stateStr == TelephonyManager.EXTRA_STATE_OFFHOOK) {
                state = TelephonyManager.CALL_STATE_OFFHOOK
            } else if (stateStr == TelephonyManager.EXTRA_STATE_RINGING) {
                state = TelephonyManager.CALL_STATE_RINGING
            }


            onCallStateChanged(context, state, number)
        }
    }


    //Incoming call-  goes from IDLE to RINGING when it rings, to OFFHOOK when it's answered, to IDLE when its hung up
    //Outgoing call-  goes from IDLE to OFFHOOK when it dials out, to IDLE when hung up
    fun onCallStateChanged(context: Context, state: Int, number: String?) {
        contact_name=context.getContactDisplayNameByNumber(number!!)
        if (lastState == state) {
            //No change, debounce extras
            return
        }
        when (state) {
            TelephonyManager.CALL_STATE_RINGING -> {
                isIncoming = true
                callStartTime = Date()
                savedNumber = number
                onIncomingCallStarted(context, number, callStartTime!!)
            }
            TelephonyManager.CALL_STATE_OFFHOOK ->
                //Transition of ringing->offhook are pickups of incoming calls.  Nothing done on them
                if (lastState != TelephonyManager.CALL_STATE_RINGING) {
                    isIncoming = false
                    callStartTime = Date()
                    onOutgoingCallStarted(context, savedNumber, callStartTime!!)
                }
            TelephonyManager.CALL_STATE_IDLE ->
                //Went to idle-  this is the end of a call.  What type depends on previous state(s)
                if (lastState == TelephonyManager.CALL_STATE_RINGING) {
                    //Ring but no pickup-  a miss
                    onMissedCall(context, savedNumber, callStartTime)
                } else if (isIncoming) {
                    onIncomingCallEnded(context, savedNumber, callStartTime, Date())
                } else {
                    onOutgoingCallEnded(context, number, callStartTime, Date())
                }
        }
        lastState = state
    }

    private fun onOutgoingCallEnded(context: Context, savedNumber: String?, callStartTime: Date?, date: Date) {
        var number = savedNumber
        if (!number!!.contains("+")) number = "+91$number"
        end_time = System.currentTimeMillis()
        //Total time talked =
        val duration = end_time - start_time
        message = Constants.OUTGOING
        3.runDelayed() {
            var information =
                Information(
                    message = message,
                    contact_name = contact_name,
                    contact_number = number + "",
                    latlng = latLng,
                    duration = duration,
                    admin_email = client.admin_email,
                    admin_identification = client.client_identification

                )
            firestoreDataGetter.saveInformation(information, this)
        }

    }

    private fun onIncomingCallEnded(context: Context, savedNumber: String?, callStartTime: Date?, date: Date) {
        var number = savedNumber
        if (!number!!.contains("+")) number = "+91$number"
        end_time = System.currentTimeMillis()
        //Total time talked =
        val duration = end_time - start_time
        message = Constants.INCOMING
        3.runDelayed() {
            var information =
                Information(
                    message = message,
                    contact_name = contact_name,
                    contact_number = number + "",
                    latlng = latLng,
                    duration = duration,
                    admin_email = client.admin_email,
                    admin_identification = client.client_identification
                )
            firestoreDataGetter.saveInformation(information, this)
        }


    }

    private fun onMissedCall(context: Context, savedNumber: String?, callStartTime: Date?) {
        var number = savedNumber
        end_time = System.currentTimeMillis()
        //Total time talked =
        val duration = end_time - start_time
        if (!number!!.contains("+")) number = "+91$number"
        message =Constants.MISSED
        3.runDelayed() {
            var information =
                Information(
                    message = message,
                    duration = duration,
                    contact_name = contact_name,
                    contact_number = number + "",
                    latlng = latLng,
                    admin_email = client.admin_email,
                    admin_identification = client.client_identification
                )
            firestoreDataGetter.saveInformation(information, this)
        }

    }

    private fun onOutgoingCallStarted(context: Context, savedNumber: String?, callStartTime: Date) {
        start_time = System.currentTimeMillis()

    }

    private fun onIncomingCallStarted(context: Context, number: String?, callStartTime: Date) {
        start_time = System.currentTimeMillis()
    }

    override fun onFailure(message: String) {
        message.toast()
    }

    override fun onSuccessForSigleObject(it: Task<Void>) {
        "information Saved Success".log("status")
    }

    override fun onSuccessForArrayObject(it: Task<QuerySnapshot>) {
    }


    companion object {

        //The receiver will be recreated whenever android feels like it.  We need a static variable to remember data between instantiations
        private var lastState = TelephonyManager.CALL_STATE_IDLE
        private var callStartTime: Date? = null
        private var isIncoming: Boolean = false
        private var savedNumber: String? = null  //because the passed incoming is only valid in ringing
        private var latLng = ""
        private var message = ""
        private var contact_name=""
        private var start_time: Long = 0
        private var end_time: Long = 0

    }


    fun initLocation(context: Context) {
        //create a callback
        val callback = object : LocationProvider.LocationCallback {

            override fun locationRequestStopped() {
            }

            override fun onNewLocationAvailable(lat: Float, lon: Float) {
                latLng = "$lat,$lon"
                //"location $lat and $lon enabled".toast()
                //location update
            }

            override fun locationServicesNotEnabled() {
               // "location not enabled".toast()
                //failed finding a location
            }

            override fun updateLocationInBackground(lat: Float, lon: Float) {
                latLng = "$lat,$lon"
                //if a listener returns after the main locationAvailable callback, it will go here
            }

            override fun networkListenerInitialised() {
                //when the library switched from GPS only to GPS & network
            }
        }

        //initialise an instance with the two required parameters
        val locationProvider = LocationProvider.Builder()
            .setContext(context)
            .setListener(callback)
            .create()

        //start getting location
        locationProvider.requestLocation()
    }
}
