package com.example.manish.locationfinderUserApp.keys

object FirestoreKeys{
    val ADMINS="admins"
    val INFORMTION="information"
    val DATA="data"
    val CLIENTS="clients"
    val CLIENT_DATA="clients_data"
}