package com.example.manish.locationfinderUserApp.keys

object PrefrencesKeys{

    val CLIENT_IDENTIFICATION="identification"
    val HIDE="Hide"
    val CANCEL="Cancel"
    val ADMIN_EMAIL="admin_email"
    val CLIENT="client"
    val LOGED_IN="loged_id"

}