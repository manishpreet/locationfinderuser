package com.example.manish.locationfinderUserApp.extension

import com.example.manish.locationfinderUserApp.models.Information
import io.realm.Realm
import io.realm.Sort


fun Information.saveInformation() {
    var realm = Realm.getDefaultInstance()
    realm.beginTransaction()
    realm.insertOrUpdate(this)
    realm.commitTransaction()
    realm.close()
}


fun Long.deleteInformation() {
    var realm = Realm.getDefaultInstance()
    realm.executeTransaction { realm ->
        val result = realm.where(Information::class.java!!).equalTo("timestemp", this).findAll()
        result.deleteAllFromRealm()
    }
}


fun getUnsaveInformation(): MutableList<Information>? {
    var realm = Realm.getDefaultInstance()
    val dd = realm.where(Information::class.java).sort("timestemp", Sort.DESCENDING).findAll()
    return realm.copyToRealmOrUpdate(dd)

}


