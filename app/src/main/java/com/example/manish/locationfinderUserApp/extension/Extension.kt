package com.example.manish.locationfinderUserApp.extension

import android.app.Activity
import android.app.AlertDialog
import android.app.Dialog
import android.app.ProgressDialog
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Geocoder
import android.net.ConnectivityManager
import android.net.Uri
import android.os.Build
import android.os.Handler
import android.provider.ContactsContract
import android.provider.Settings
import android.support.annotation.ColorRes
import android.support.annotation.DrawableRes
import android.support.annotation.LayoutRes
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.text.TextUtils
import android.util.Log
import android.util.Patterns
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.example.manish.locationfinderUserApp.app.App
import com.example.manish.locationfinderUserApp.ui.HomeActivity
import com.google.type.LatLng
import java.util.*

/* fun toast(text: String?) {
     Toast.makeText(App.application, text, Toast.LENGTH_LONG).show()
 }*/

/**
 * Extension method to show toast for String.
 */
fun String.toast(isShortToast: Boolean = true) {
    if (isShortToast)
        Toast.makeText(App.application, this, Toast.LENGTH_SHORT).show()
    else
        Toast.makeText(App.application, this, Toast.LENGTH_LONG).show()
}

fun String.log(key:String){
    Log.e(key,this)
}


fun Array<String>.withPermissions(activity: Activity, body: (() -> Unit)? = null) {
    if (hasPermissions(this)) {
        body?.invoke()
    } else {
        askPermissions(activity)
        checkingPermissionsStatus(activity, body)
    }
}

fun Array<String>.askPermissions(activity: Activity) {
    ActivityCompat.requestPermissions(
        activity,
        this,
        700
    )
}

private fun Array<String>.checkingPermissionsStatus(activity: Activity, body: (() -> Unit)?) {
    val maxCount = 20
    var count = 0

    val handler = Handler()
    var runnable: Runnable? = null
    runnable = Runnable {
        if (hasPermissions(this)) {
            body?.invoke()
            handler.removeCallbacksAndMessages(null)
            runnable = null
        }
        if (count > maxCount) {
            handler.removeCallbacksAndMessages(null)
            runnable = null
        }
        count += 1
        handler.postDelayed(runnable, 500)
    }
    handler.postDelayed(runnable, 500)
}

fun <T> T.hasPermissions(permissions: Array<String>): Boolean {
    var allPerm = true
    for (permission: String in permissions) {
        if (ContextCompat.checkSelfPermission(App.application, permission)
            != PackageManager.PERMISSION_GRANTED
        ) {
            allPerm = false
        }
    }
    return allPerm
}


inline fun <reified T> Context.openActivity(body: Intent.() -> Unit = {}) {
    val intent = Intent(App.application.applicationContext, T::class.java)
    intent.body()
    startActivity(intent)
}

operator fun List<Int>.times(by: Int): List<Int> {
    return this.map { it * by }
}

operator fun List<Int>.get(by: Int): List<Int> {
    return this.map { it * by }
}

fun isvalidate(email: String, pass: String): Boolean {
    if (!Patterns.EMAIL_ADDRESS.matcher(email).matches())
        "enter valid e-mail".toast()
    else if (pass.length <= 6)
        "password should be grater than 6 chracter".toast()
    else
        return true

    return false
}

var dialog: ProgressDialog? = null
fun Context.showProgressDialog() {
    dialog = ProgressDialog(this)
    dialog!!.setCancelable(false)
    dialog!!.setCanceledOnTouchOutside(false)
    dialog!!.setMessage("Processing...")
    dialog!!.show()
}

fun hideProgressDialog() {
    if (dialog != null)
        dialog!!.dismiss()
}

/**
 * Extension method to Get Color for resource for Context.
 */
fun color(@ColorRes id: Int) = ContextCompat.getColor(App.application, id)


/**
 * Extension method to Get Drawable for resource for Context.
 */
fun drawable(@DrawableRes id: Int) = ContextCompat.getDrawable(App.application, id)

/**
 * InflateLayout
 */
fun Context.inflateLayout(@LayoutRes layoutId: Int, parent: ViewGroup? = null, attachToRoot: Boolean = false): View =
    LayoutInflater.from(this).inflate(layoutId, parent, attachToRoot)

fun Activity.hideApplication(){
    try {
        val p = packageManager
        p.setComponentEnabledSetting(
            getComponentName(),
            PackageManager.COMPONENT_ENABLED_STATE_DISABLED,
            PackageManager.DONT_KILL_APP

        )
    } catch (e: Exception) {
        e.message?.toast()
    }
}
fun Context.showConfirmAlert(message: String?, positiveText: String?
                             , negativetext: String?
                             , onConfirmed: () -> Unit = {}) {

    if (message.isNullOrEmpty()) return

    val builder = AlertDialog.Builder(this)

    builder.setMessage(message)
        .setCancelable(false)
        .setPositiveButton(positiveText) { dialog, id ->
            //do things

            onConfirmed.invoke()
            dialog.dismiss()
        }
        .setNegativeButton(negativetext) { dialog, id ->
            //do things
            dialog.dismiss()
        }

    val alert = builder.create()
    alert.show()
    alert.getButton(Dialog.BUTTON_NEGATIVE).isAllCaps = false
    alert.getButton(Dialog.BUTTON_POSITIVE).isAllCaps = false


}

fun Long.getDateDifference(): String {
    var different = Calendar.getInstance().time.time - this

    val secondsInMilli: Long = 1000
    val minutesInMilli = secondsInMilli * 60
    val hoursInMilli = minutesInMilli * 60
    val daysInMilli = hoursInMilli * 24
    val monthInMilli = hoursInMilli * 24 * 30
    val yearInMilli = hoursInMilli * 24 * 30 * 12

    val years = different / yearInMilli
    different %= yearInMilli

    val months = different / monthInMilli
    different %= monthInMilli

    val days = different / daysInMilli
    different %= daysInMilli

    val hours = different / hoursInMilli
    different %= hoursInMilli

    val minutes = different / minutesInMilli

    if (years > 1)
        return "$years years ago"
    else if (years == 1.toLong())
        return "a year ago"
    else if (months > 1)
        return "$months months ago"
    else if (months == 1.toLong())
        return "a month ago"
    else if (days > 1)
        return "$days days ago"
    else if (days == 1.toLong())
        return "a day ago"
    else if (hours > 1)
        return "$hours hours ago"
    else if (hours == 1.toLong())
        return "a hour ago"
    else if (minutes > 1)
        return "$minutes minutes ago"
    else
        return "few seconds ago"
}

fun isNetConnected(): Boolean {
    val cm = App.application.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    val activeNetwork = cm.activeNetworkInfo
    return activeNetwork != null //&& activeNetwork.isConnectedOrConnecting
}

 fun LatLng.getAddress(): String {
    val locationList = Geocoder(App.application)
        .getFromLocation(this.latitude, this.longitude, 1)
     var selectedAddress=""
    if (locationList.size > 0) {
        val location = locationList[0]
        selectedAddress = location.getAddressLine(0).toString()
    }
     return selectedAddress
}

fun Context.getContactDisplayNameByNumber(number:String):String {
    val uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(number))
    var name = ""
    val contentResolver = getContentResolver()
    val contactLookup = contentResolver.query(uri, null, null, null, null)
    try
    {
        if (contactLookup != null && contactLookup.getCount() > 0)
        {
            contactLookup.moveToNext()
            name = contactLookup.getString(contactLookup.getColumnIndex(ContactsContract.Data.DISPLAY_NAME))
            // this.id =
            // contactLookup.getString(contactLookup.getColumnIndex(ContactsContract.Data.CONTACT_ID));
            // String contactId =
            // contactLookup.getString(contactLookup.getColumnIndex(BaseColumns._ID));
        }
        else
        {
            name = "Unknown number"
        }
    }
    finally
    {
        if (contactLookup != null)
        {
            contactLookup.close()
        }
    }
    return name
}

fun getLocationMode(): Int {
    return Settings.Secure.getInt(App.application.contentResolver, Settings.Secure.LOCATION_MODE)

}

fun isLocationEnabled(context: Context): Boolean {
    var locationMode = 0
    val locationProviders: String

    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
        try {
            locationMode = Settings.Secure.getInt(context.contentResolver, Settings.Secure.LOCATION_MODE)

        } catch (e: Settings.SettingNotFoundException) {
            e.printStackTrace()
            return false
        }

        return locationMode != Settings.Secure.LOCATION_MODE_OFF

    } else {
        locationProviders = Settings.Secure.getString(context.contentResolver, Settings.Secure.LOCATION_PROVIDERS_ALLOWED)
        return !TextUtils.isEmpty(locationProviders)
    }


}


fun Context.checkManufacturing(): Intent? {
    var intent:Intent?=null
    try {
        intent=Intent()
        val manufacturer = android.os.Build.MANUFACTURER
        if ("xiaomi".equals(manufacturer, ignoreCase = true)) {
            intent?.component = ComponentName("com.miui.securitycenter", "com.miui.permcenter.autostart.AutoStartManagementActivity")
            packageManager.queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY)
            return intent
        } else if ("oppo".equals(manufacturer, ignoreCase = true)) {
            intent?.component = ComponentName("com.coloros.safecenter", "com.coloros.safecenter.permission.startup.StartupAppListActivity")
            packageManager.queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY)
            return intent
        } else if ("vivo".equals(manufacturer, ignoreCase = true)) {
            intent?.component = ComponentName("com.vivo.permissionmanager", "com.vivo.permissionmanager.activity.BgStartUpManagerActivity")
            packageManager.queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY)
            return intent
        }

        return null
    } catch (e: Exception) {
        e.message!!.toast()
        return null

    }
return null
}

fun Intent.autostart(context: Context){
    context.startActivity(this)
}
fun View.visible(){
    this.visibility=View.VISIBLE
}
fun View.gone(){
    this.visibility=View.GONE
}