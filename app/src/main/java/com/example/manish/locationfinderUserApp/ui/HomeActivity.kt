package com.example.manish.locationfinderUserApp.ui

import android.annotation.SuppressLint
import android.os.Bundle
import android.provider.Settings.Secure
import android.support.v7.app.AppCompatActivity
import com.example.manish.locationfinderUserApp.R
import com.example.manish.locationfinderUserApp.extension.*
import com.example.manish.locationfinderUserApp.intracters.FirestoreDataGetter
import com.example.manish.locationfinderUserApp.intracters.FirestoreListner
import com.example.manish.locationfinderUserApp.keys.PrefrencesKeys
import com.example.manish.locationfinderUserApp.models.Client
import com.google.android.gms.tasks.Task
import com.google.firebase.firestore.QuerySnapshot
import kotlinx.android.synthetic.main.activity_home.*


class HomeActivity : AppCompatActivity() {


    var device_id = ""
    val firestoreDataGetter by lazy { FirestoreDataGetter() }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)


        device_id = Secure.getString(
            this.contentResolver,
            Secure.ANDROID_ID
        )

        txtHideApplication.setOnClickListener {
            if (getPrefrence(PrefrencesKeys.LOGED_IN, false)) {
                hideApplication()
                finish()
            } else {
                "login to hide app".toast()
            }
        }
        txtHideApplicationEmpty.setOnClickListener {
            hideApplication()
            finish()
        }
        txtSave.setOnClickListener {
            var clientIdentification = etIdentification.text.toString().trim()
            var adminEmail = etAdminEmail.text.toString().trim()

            if (clientIdentification.isNotEmpty() || adminEmail.isNotEmpty())
                saveClient(
                    Client(
                        client_identification = clientIdentification,
                        admin_email = adminEmail, device_id = device_id!!
                    )
                )
            else
                "Enter Someting please".toast()
        }
        if (getPrefrence(PrefrencesKeys.LOGED_IN, false)) {
            "user loged in".log("user")
            layoutEmpty.visible()
            layoutLogin.gone()
        } else {
            checkAvailebility(device_id)
        }


    }


    @SuppressLint("MissingPermission")
    private fun checkAvailebility(device_id: String?) {
        showProgressDialog()
        firestoreDataGetter.validateIdentification(device_id, object : FirestoreListner {
            override fun onFailure(message: String) {
                hideProgressDialog()
                message.toast()
            }

            override fun onSuccessForSigleObject(it: Task<Void>) {}
            override fun onSuccessForArrayObject(it: Task<QuerySnapshot>) {
                hideProgressDialog()
                if (it.result?.size() == 0)
                    "device_id not exist".log("list")
                else {
                    it.result?.forEach {
                        var client = it.toObject(Client::class.java)
                        setprefrenceObject(PrefrencesKeys.CLIENT, client)
                        setPrefrence(PrefrencesKeys.LOGED_IN, true)
                        layoutEmpty.visible()
                        layoutLogin.gone()
                        "previous user registered".toast()
                    }
                }
            }
        })


    }

    private fun saveClient(client: Client) {
        showProgressDialog()
        firestoreDataGetter.saveClient(client, object : FirestoreListner, () -> Unit {

            override fun invoke() {
                hideApplication()
                finish()
            }

            override fun onFailure(message: String) {
                hideProgressDialog()
                message.toast()
            }

            override fun onSuccessForSigleObject(it: Task<Void>) {
                etAdminEmail.setText("")
                etIdentification.setText("")
                setPrefrence(PrefrencesKeys.LOGED_IN, true)
                setprefrenceObject(PrefrencesKeys.CLIENT, client)
                hideProgressDialog()
                layoutEmpty.visible()
                layoutLogin.gone()
                showConfirmAlert(
                    getString(R.string.message),
                    PrefrencesKeys.HIDE, PrefrencesKeys.CANCEL, this
                )
            }

            override fun onSuccessForArrayObject(it: Task<QuerySnapshot>) {}
        })
    }
}
