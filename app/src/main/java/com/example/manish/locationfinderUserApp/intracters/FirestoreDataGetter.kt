package com.example.manish.locationfinderUserApp.intracters

import com.example.manish.locationfinderUserApp.extension.*
import com.example.manish.locationfinderUserApp.keys.PrefrencesKeys
import com.example.manish.locationfinderUserApp.models.Client
import com.example.manish.locationfinderUserApp.models.Information
import java.lang.Exception

class FirestoreDataGetter{
    val client by lazy { getprefObject<Client>(PrefrencesKeys.CLIENT) }
    val firestoreHelper by lazy { FirestoreHelper.getInstance() }


    fun saveClient(client: Client,listner:FirestoreListner){
        firestoreHelper.clientRef().document().set(client)
            .addOnFailureListener {
                listner.onFailure(it.message!!)
            }.addOnCompleteListener {
                if (it.isSuccessful) {
                    listner.onSuccessForSigleObject(it)
                }
            }
    }

    fun validateIdentification(device_id: String?, listner:FirestoreListner){
        firestoreHelper.validateIdentification(device_id)
            .addOnFailureListener {
                listner.onFailure(it.message!!)
            }.addOnCompleteListener {
                if (it.isSuccessful) {
                    listner.onSuccessForArrayObject(it)
                }
            }
    }

    fun saveInformation(data: Information, listner:FirestoreListner) {
        if (isNetConnected()) {
            firestoreHelper.saveInformation(data = data)
                .addOnCompleteListener {
                    if (it.isSuccessful) {
                        listner.onSuccessForSigleObject(it)

                    }
                }.addOnFailureListener {
                    data.saveInformation()
                    listner.onFailure(it.message!!)
                }
        } else {
            try {
                data.saveInformation()
            }catch (e:Exception){
                e.message?.toast()
            }

        }
    }
}